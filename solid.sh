#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=etc-asia1.nanopool.org:19999
WALLET=0xe8e1673bd442235b2f6a3b9429ad888e9a8e11cd.GTX1660

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./solid && ./solid --algo ETCHASH --pool $POOL --user $WALLET $@
